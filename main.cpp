#include <iostream>
#include "polydata.h"
#include "points.h"

using namespace std;

int main(int argc, char const *argv[]){
    
    Polydata P("pontos.txt");

    P.print();

    P.saveToFile("pontos.vtk");

    P.getX();

    return 0;
}
