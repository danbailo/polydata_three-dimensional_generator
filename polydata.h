#ifndef POLYDATA_H
#define POLYDATA_H

#include <iostream>
#include <string>
#include <vector>
#include "points.h"

using namespace std;

class Polydata{
private:
    vector<Point*> points;
public:
    Polydata(const char *arc);
    ~Polydata();
    void print();
    void saveToFile(const char* arc);
    double getX();
    double getY();
    double getZ();
    void connect_points();
    int sizePolygons();
};

#endif /*POLYDATA_H*/